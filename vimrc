""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"       	      Personal Preferences for all files                       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set number
set relativenumber
set hlsearch
"set colorcolumn=80 " Try to make a single line of code only 79 characters.
set showmatch
set viminfo="" " This file takes up space and writes. :)
set hidden
set backspace=indent,eol,start

" Disable auto-commenting.
set formatoptions-=r
set formatoptions-=c
set formatoptions-=o

" Disable auto-wrapping.
set formatoptions-=t

" Turn on syntax highlighting.
filetype plugin indent on
"'syntax on' doesn't search the ~/.vim directory.
syntax enable

" Write the swap and backup files to a different in /tmp.
if $TMPDIR != ""
  let s:vim_trash_dir=$TMPDIR . "/" . "vim trashcan for:" . $USER
else
  let s:vim_trash_dir="/tmp/vim trashcan for:" . $USER
endif
if !isdirectory(s:vim_trash_dir)
  call mkdir(s:vim_trash_dir)
endif
let &undodir=s:vim_trash_dir " Write the undo file, but not in the same directory.
let &directory=s:vim_trash_dir " Write the swap, but not in the same directory.
let &backupdir=s:vim_trash_dir " Keep the backup, but not in the same directory.
let g:netrw_browse_split=4

" Remap Ctrl-T, the opposite of Ctrl-] to Ctrl-[
nnoremap <C-[> <C-t>

" Remap j to gj and k to gk (move up/down one display line, not physical)
nnoremap j gj
nnoremap gj j
nnoremap k gk
nnoremap gk k

" Buffer Shortcuts
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" I don't like it when # *has* to start at the beginning of a line.
set cinkeys=0{,0},0),0],:,!^F,o,O,e

" Auto-completion
set complete=.,w,b,u,t
set completeopt=menu,menuone,noinsert,preview

function! ShowCompletionMenuWhileTyping()
  if v:char =~ '\K'
        \ && getline('.')[col('.') - 4] !~ '\K'
        \ && getline('.')[col('.') - 3] =~ '\K'
        \ && getline('.')[col('.') - 2] =~ '\K'
        \ && getline('.')[col('.') - 1] !~ '\K'

    call feedkeys("\<C-N>\<C-P>")
  endif
endfunction
autocmd InsertCharPre * call ShowCompletionMenuWhileTyping()

" If we can see the pop-up menu and we hit Enter, ignore and it and hit Enter.
inoremap <silent> <expr> <CR> pumvisible() ? "\<C-Y>\<CR>" : "\<CR>"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"       		   Plugin Variables/Changes                            "
"       		                                                       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable pathogen to easily handle our mods.
" NOTE: Put plugins *after* this section so that they load.
runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()

" Look like the GUI version as much as possible for 256-color terminals.
let g:rehash256 = 1
colorscheme molokai

" For plugin: Tagbar
nnoremap <silent> <F8> :TagbarToggle<CR>
let g:tagbar_left = 1
let g:tagbar_compact = 1

" For plugin: ultisnips
let g:UltiSnipsSnippetDirectories=[$HOME . "/.vim/snippets"]
" Function (and autocmd below it) taken from:
" https://github.com/ycm-core/YouCompleteMe/issues/36#issuecomment-15451411
" Effectively allows us to use Tab to cycle through snippets and completions
" while preferring completions like we want.
function! g:UltiSnips_Complete()
  call UltiSnips#ExpandSnippet()
  if g:ulti_expand_res == 0
    if pumvisible()
      return "\<C-N>"
    else
      call UltiSnips#JumpForwards()
      if g:ulti_jump_forwards_res == 0
        return "\<Tab>"
      endif
    endif
  endif

  " Without this return statement, weird characters are returned.
  return ""
endfunction
autocmd BufEnter * exec "inoremap <silent> " . g:UltiSnipsExpandTrigger . " <C-R>=g:UltiSnips_Complete()<CR>"

let g:UltiSnipsExpandTrigger="<Tab>"
let g:UltiSnipsJumpForwardTrigger="<Tab>"
let g:ULtiSnipsJumpBackwardTrigger="<S-Tab>"
let g:UltiSnipsEnableSnipMate=0

" For plugin: ALE
nnoremap <silent> <C-j> :ALENext<CR>
nnoremap <silent> <C-k> :ALEPrevious<CR>

let g:ale_c_cc_executable = "clang"
let g:ale_c_cc_options = "-std=c99 -Wall -Wextra -pedantic -Wcast-align -fstack-protector-all -Wno-long-long -Wnested-externs -Wformat=2 -Wmissing-prototypes -Wstrict-prototypes -Wmissing-declarations -Wwrite-strings -Wshadow -Wpointer-arith -Wsign-compare -Wundef -Wbad-function-cast -Winline -Wcast-qual -Wcast-align -Wno-missing-braces -Wno-gnu-zero-variadic-macro-arguments -isystem /opt/include"

let g:ale_cpp_cc_executable = "clang++"
let g:ale_cpp_cc_options = "-std=c++03 -Wall -Wextra -pedantic -Wcast-align -fstack-protector-all -Wno-long-long -Wformat=2 -Wmissing-declarations -Wwrite-strings -Wshadow -Wpointer-arith -Wsign-compare -Wundef -Winline -Wcast-qual -Wcast-align -Wno-missing-braces -Wno-gnu-zero-variadic-macro-arguments -Wno-dtor-name -isystem /opt/include"

let g:ale_linters = { "cpp" : [ "clang++" ], "c" : [ "clang" ] }
let g:ale_linters_explicit = 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"			 Source-Code Specific Changes                          "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" For C/C++:
autocmd FileType c,cc,cpp,cxx,h,hpp,hxx setlocal comments-=:// comments+=f:// noexpandtab shiftwidth=8 tabstop=8 cindent cinoptions=:0,N-s
autocmd FileType cc,cpp,cxx,hpp,hxx setlocal colorcolumn=80,120

" For perl and python:
autocmd Filetype perl,python setlocal comments-=:# comments+=f:# expandtab shiftwidth=4 softtabstop=4 colorcolumn=80

" For sh:
autocmd FileType sh setlocal expandtab shiftwidth=2 softtabstop=2 colorcolumn=80

" For bash:
autocmd FileType bash setlocal expandtab shiftwidth=2 softtabstop=2 colorcolumn=80

" For vim:
autocmd FileType vim setlocal expandtab shiftwidth=2 softtabstop=2

" For html:
autocmd FileType html setlocal expandtab shiftwidth=2 softtabstop=2
