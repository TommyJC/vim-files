# vim-files #
These are my personal vim files and plugins that I use (as submodules).

## Installation ##
~~~
git clone https://bitbucket.org/TommyJC/vim-files "$HOME"/.vim
cd -- "$HOME"/.vim &&
git submodule init &&
git submodule update || exit
for file in vimrc gvimrc; do
  ln -s -- "$HOME/.$file" "$file"
done
~~~
